package com.amx.andromedanew;

import android.content.Intent;
import android.nfc.Tag;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class LoginVerifikasi extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private PhoneAuthProvider.ForceResendingToken forceResendingToken;
    private TextView tv_tmbl1, tv_tmbl2, tv_tmbl3, tv_tmbl4, tv_tmbl5, tv_tmbl6, tv_tmbl7, tv_tmbl8, tv_tmbl9, tv_tmbl0;
    private static final long START_TIME_IN_MILLIS = 31000;
    private CountDownTimer mCountDownTimer;
    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;

    static final String EXTRA_MOBILE = "extra_mobile";
    TextView tv_resend, tv_timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_verifikasi);
        inir();
        Pinview pinview = new Pinview(this);
        pinview = findViewById(R.id.pinview);
        mAuth = FirebaseAuth.getInstance();
        tv_resend = findViewById(R.id.textView9);
        tv_timer = findViewById(R.id.textView5);
        Intent intent = getIntent();
        String mobile = intent.getStringExtra(EXTRA_MOBILE);
        send_sms(mobile);
        tv_resend.setOnClickListener(this);
//        pinview.setValue("1");
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                verifyOtp(pinview.getValue());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.textView9) {
            Intent intent = getIntent();
            String mobile = intent.getStringExtra(EXTRA_MOBILE);
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    mobile,        // Phone number to verify
                    30,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallback,
                    forceResendingToken);        // OnVerificationStateChangedCallbacks
            startTimer();
        }
        switch (v.getId()) {
            case R.id.textView10:

                break;
        }
    }

    public void send_sms(String mobile) {
        setUpVerificationCallbacks();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(mobile, 30, TimeUnit.SECONDS, this, mCallback);
        startTimer();
    }

    public void setUpVerificationCallbacks() {
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(getApplicationContext(), "Verifikasi Berhasil", Toast.LENGTH_SHORT).show();

                startActivity(new Intent(LoginVerifikasi.this, Main2Activity.class));

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken Token) {
//            super.onCodeSent(s, forceResendingToken);
                mVerificationId = s;
                Toast.makeText(getApplicationContext(), "Code sent to the number", Toast.LENGTH_SHORT).show();
                forceResendingToken = Token;
            }
        };
    }

    public void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Kode Verifikasi benar", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(LoginVerifikasi.this, Main2Activity.class));
                } else {
                    Toast.makeText(getApplicationContext(), "Kode Verifikasi salah", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void verifyOtp(String codenew) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, codenew);
        signInWithPhoneAuthCredential(credential);
    }

    public void inir() {
        tv_tmbl1 = findViewById(R.id.textView10);
        tv_tmbl2 = findViewById(R.id.textView20);
        tv_tmbl3 = findViewById(R.id.textView22);
        tv_tmbl4 = findViewById(R.id.textView23);
        tv_tmbl5 = findViewById(R.id.textView24);
        tv_tmbl6 = findViewById(R.id.textView25);
        tv_tmbl7 = findViewById(R.id.textView26);
        tv_tmbl8 = findViewById(R.id.textView28);
        tv_tmbl9 = findViewById(R.id.textView27);
        tv_tmbl0 = findViewById(R.id.textView11);
    }

    private void updateCountDownText() {
        int seconds = (int) (mTimeLeftInMillis / 1000);
        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", 0, seconds);
        tv_timer.setText(timeLeftFormatted);
    }

    private void startTimer() {
        mCountDownTimer = new CountDownTimer(31000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();

            }

            @Override
            public void onFinish() {
//                Toast.makeText(getApplicationContext(), "Selesai", Toast.LENGTH_SHORT).show();
                tv_resend.setEnabled(true);


            }
        }.start();
    }

}



