package com.amx.andromedanew;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class ReceiverSms extends BroadcastReceiver {

    private static final String TAG = "Message recieved";
    static String cd;
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle pudsBundle = intent.getExtras();
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        SmsMessage messages =SmsMessage.createFromPdu((byte[]) pdus[0]);

        // Start Application's  MainActivty activity

        Intent smsIntent=new Intent(context,MainActivity.class);

        smsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        smsIntent.putExtra("MessageNumber", messages.getOriginatingAddress());


        String code=messages.getMessageBody();
        String [] angka= code.split(":");
        cd =angka[1].trim();
        smsIntent.putExtra("code", cd);
       // context.startActivity(smsIntent);

        // Get the Sender Message : messages.getMessageBody()
        // Get the SenderNumber : messages.getOriginatingAddress()

       // Toast.makeText(context, "SMS Received From :"+messages.getOriginatingAddress()+"\n"+cd, Toast.LENGTH_LONG).show();
    }
}
