package com.amx.andromedanew;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "PhoneAuth";
    private String phoneVerificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationCallbacks;
    private PhoneAuthProvider.ForceResendingToken resendToken;
    private FirebaseAuth fbAuth;
    private String verificationId;


    CountryCodePicker ccp;
    Button btn;
    EditText etphone;
    TextView tvshow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        ccp.registerCarrierNumberEditText(etphone);
        fbAuth = FirebaseAuth.getInstance();

        btn.setOnClickListener(this);

//sendVerificationCode();

    }

    private void init() {
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        etphone = (EditText) findViewById(R.id.phonenumber);
        btn = (Button) findViewById(R.id.verify);

       // tvshow = (TextView) findViewById(R.id.textView3);
    }

    @Override
    public void onClick(View v) {
        if (v == btn) {

            String number = ccp.getFullNumberWithPlus();
           // tvshow.setText(number);
            if (number.isEmpty()) {
                etphone.setError("Masukkan Nomer Handphone anda");
                etphone.requestFocus();
                return;
            }
            Intent intent = new Intent(MainActivity.this, timer.class);
            intent.putExtra(LoginVerifikasi.EXTRA_MOBILE, number);
            startActivity(intent);
        }

    }
}